use cgmath::{self, Deg, Matrix3, Matrix4, Point3, Quaternion};

#[derive(Debug, Clone)]
pub struct Camera<S> {
    pub width: S,
    pub height: S,
    quaternion: Quaternion<S>,
    translation: Point3<S>,
    projection: Projection<S>,
}

#[derive(Debug, Clone)]
pub enum Projection<S> {
    Perspective {
        fovy: Deg<S>,
        near: S,
        far: S,
    },
    Ortho {
        left: S,
        right: S,
        bottom: S,
        top: S,
        near: S,
        far: S,
    },
}

impl<S> Camera<S>
where
    S: cgmath::BaseFloat,
{
    pub fn new(
        width: S,
        height: S,
        quaternion: Quaternion<S>,
        translation: Point3<S>,
        projection: Projection<S>,
    ) -> Camera<S> {
        Camera {
            width,
            height,
            quaternion,
            translation,
            projection,
        }
    }

    pub fn rotation_matrix(&self) -> Matrix3<S> {
        self.quaternion.into()
    }

    pub fn modelview_matrix(&self) -> Matrix4<S> {
        let r = self.rotation_matrix();
        Matrix4::look_at_dir(self.translation, r.z, r.y)
    }

    pub fn projection_matrix(&self) -> Matrix4<S> {
        self.projection.projection_matrix(self.width, self.height)
    }

    pub fn do_pitch(&mut self, a: S) {
        self.quaternion = Quaternion::from_sv(a, self.rotation_matrix().x) * self.quaternion;
    }

    pub fn do_yaw(&mut self, a: S) {
        self.quaternion = Quaternion::from_sv(a, self.rotation_matrix().y) * self.quaternion;
    }

    pub fn do_roll(&mut self, a: S) {
        self.quaternion = Quaternion::from_sv(a, self.rotation_matrix().z) * self.quaternion;
    }

    pub fn go_right(&mut self, a: S) {
        self.translation += self.rotation_matrix().x * a;
    }

    pub fn go_up(&mut self, a: S) {
        self.translation += self.rotation_matrix().y * a;
    }

    pub fn go_forward(&mut self, a: S) {
        self.translation += self.rotation_matrix().z * a;
    }
}

impl<S: cgmath::BaseFloat> Projection<S> {
    pub fn projection_matrix(&self, width: S, height: S) -> Matrix4<S> {
        match self {
            Projection::Perspective { fovy, near, far } => {
                cgmath::perspective(*fovy, width / height, *near, *far)
            }
            Projection::Ortho {
                left,
                right,
                bottom,
                top,
                near,
                far,
            } => cgmath::ortho(*left, *right, *bottom, *top, *near, *far),
        }
    }
}
