use std::collections::HashSet;
use std::ffi::CString;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::ptr;
use std::sync::RwLock;

use cgmath::{Matrix3, Matrix4, Point2, Point3};
use gl;
use gl::types::*;

use crate::buffer::{ArrayBuffer, UniformBuffer};

/// シェーダーをコンパイル・リンクするためのコンテキスト
#[derive(Debug)]
pub struct ShaderBuilder {
    vertex_shader: Option<GLuint>,
    fragment_shader: Option<GLuint>,
}

/// コンパイルされたシェーダー
#[derive(Debug)]
pub struct Shader {
    id: GLuint,
    used_locations: RwLock<HashSet<CString>>,
}

#[derive(Debug)]
pub struct AttribLocation<'a> {
    id: GLuint,
    name: CString,
    shader: &'a Shader,
}

#[derive(Debug)]
pub struct UniformLocation<'a> {
    id: GLint,
    name: CString,
    shader: &'a Shader,
}

#[derive(Debug)]
pub struct Attrib<'a, 'b, T: ?Sized> {
    id: GLuint,
    name: CString,
    shader: &'a Shader,
    buffer: &'b ArrayBuffer<T>,
    normalized: GLboolean,
    stride: GLsizei,
}

#[derive(Debug)]
pub struct Uniform<'a, 'b, T: ?Sized> {
    id: GLint,
    name: CString,
    shader: &'a Shader,
    buffer: &'b UniformBuffer<T>,
}

impl ShaderBuilder {
    pub fn new() -> ShaderBuilder {
        ShaderBuilder {
            vertex_shader: None,
            fragment_shader: None,
        }
    }

    fn delete_shader(shader: Option<GLuint>) {
        if let Some(shader) = shader {
            unsafe {
                gl::DeleteShader(shader);
            }
        }
    }

    pub fn vertex_source(mut self, source: &str) -> Self {
        Self::delete_shader(self.vertex_shader);
        let source = CString::new(source.as_bytes()).unwrap();
        let id = unsafe { gl::CreateShader(gl::VERTEX_SHADER) };
        unsafe {
            gl::ShaderSource(id, 1, &source.as_ptr(), ptr::null());
            gl::CompileShader(id);
        }
        check_shader_compile(id).expect("Failed to compile vertex shader");
        self.vertex_shader = Some(id);
        self
    }

    pub fn vertex_file<P: AsRef<Path>>(self, path: &P) -> Self {
        let source = read_source(path.as_ref()).expect("Failed to read");
        self.vertex_source(&source)
    }

    pub fn fragment_source(mut self, source: &str) -> Self {
        Self::delete_shader(self.fragment_shader);
        let source = CString::new(source).unwrap();
        let id = unsafe { gl::CreateShader(gl::FRAGMENT_SHADER) };
        unsafe {
            gl::ShaderSource(id, 1, &source.as_ptr(), ptr::null());
            gl::CompileShader(id);
        }
        check_shader_compile(id).expect("Failed to compile fragment shader");
        self.fragment_shader = Some(id);
        self
    }

    pub fn fragment_file<P: AsRef<Path>>(self, path: &P) -> Self {
        let source = read_source(path.as_ref()).expect("Failed to read");
        self.fragment_source(&source)
    }

    pub fn link(&self) -> Shader {
        let id = unsafe { gl::CreateProgram() };
        unsafe {
            gl::AttachShader(id, self.vertex_shader.expect("vertex shader is required"));
            if let Some(fragment_shader) = self.fragment_shader {
                gl::AttachShader(id, fragment_shader);
            }
            gl::LinkProgram(id);
        }
        check_program_link(id).expect("Failed to link program");
        Shader {
            id,
            used_locations: RwLock::new(HashSet::new()),
        }
    }
}

impl Drop for ShaderBuilder {
    fn drop(&mut self) {
        Self::delete_shader(self.vertex_shader);
        Self::delete_shader(self.fragment_shader);
    }
}

fn read_source(path: &Path) -> std::io::Result<String> {
    let mut source = String::new();
    File::open(path)?.read_to_string(&mut source)?;
    Ok(source)
}

fn check_shader_compile(shader: GLuint) -> Result<(), String> {
    let mut success = gl::FALSE as GLint;
    unsafe {
        gl::GetProgramiv(shader, gl::COMPILE_STATUS, &mut success);
    }
    if success != gl::TRUE as GLint {
        let mut msg = vec![0; 1024];
        unsafe {
            gl::GetShaderInfoLog(
                shader,
                1024,
                ptr::null_mut(),
                msg.as_mut_ptr() as *mut GLchar,
            );
        }
        while let Some(c) = msg.last() {
            if *c == 0 {
                msg.pop();
            } else {
                break;
            }
        }
        Err(String::from_utf8(msg).unwrap())
    } else {
        Ok(())
    }
}

fn check_program_link(program: GLuint) -> Result<(), String> {
    let mut success = gl::FALSE as GLint;
    unsafe {
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);
    }
    if success != gl::TRUE as GLint {
        let mut msg = vec![0; 1024];
        unsafe {
            gl::GetProgramInfoLog(
                program,
                1024,
                ptr::null_mut(),
                msg.as_mut_ptr() as *mut GLchar,
            );
        }
        while let Some(c) = msg.last() {
            if *c == 0 {
                msg.pop();
            } else {
                break;
            }
        }
        Err(String::from_utf8(msg).unwrap())
    } else {
        Ok(())
    }
}

impl Shader {
    pub fn attrib_location(&self, name: &str) -> Option<AttribLocation> {
        let name = CString::new(name).unwrap();
        if self.used_locations.read().unwrap().contains(&name) {
            return None;
        }
        let id = unsafe { gl::GetAttribLocation(self.id, name.as_ptr() as *const GLchar) };
        if id < 0 {
            None
        } else {
            self.used_locations.write().unwrap().insert(name.clone());
            Some(AttribLocation {
                id: id as GLuint,
                name,
                shader: self,
            })
        }
    }

    pub fn uniform_location(&self, name: &str) -> Option<UniformLocation> {
        let name = CString::new(name).unwrap();
        if self.used_locations.read().unwrap().contains(&name) {
            return None;
        }
        let id = unsafe { gl::GetUniformLocation(self.id, name.as_ptr() as *const GLchar) };
        if id < 0 {
            None
        } else {
            self.used_locations.write().unwrap().insert(name.clone());
            Some(UniformLocation {
                id,
                name,
                shader: self,
            })
        }
    }

    fn drop_location(&self, name: &CString) {
        self.used_locations.write().unwrap().remove(name);
    }

    pub fn use_program(&self) {
        unsafe { gl::UseProgram(self.id) }
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}

impl<'a> AttribLocation<'a> {
    pub fn bind_buffer<'b, T>(
        self,
        buffer: &'b ArrayBuffer<T>,
        normalized: bool,
        stride: GLsizei,
    ) -> Attrib<'a, 'b, T> {
        Attrib {
            id: self.id,
            // `clone` needs because of `impl Drop`
            name: self.name.clone(),
            shader: self.shader,
            buffer,
            normalized: if normalized { gl::TRUE } else { gl::FALSE },
            stride,
        }
    }
}

impl Drop for AttribLocation<'_> {
    fn drop(&mut self) {
        self.shader.drop_location(&self.name);
    }
}

impl<T: ?Sized> Attrib<'_, '_, T> {
    pub fn disable(&self) {
        unsafe { gl::DisableVertexAttribArray(self.id) };
    }
}

impl<T: ?Sized> Drop for Attrib<'_, '_, T> {
    fn drop(&mut self) {
        self.disable();
        self.shader.drop_location(&self.name);
    }
}

macro_rules! impl_attrib_enable {
    ($rs_ty:ty, $size:expr, $gl_ty:path) => {
        impl Attrib<'_, '_, [$rs_ty]> {
            pub fn enable(&self) {
                unsafe {
                    gl::EnableVertexAttribArray(self.id);
                    gl::VertexAttribPointer(
                        self.id,
                        $size,
                        $gl_ty,
                        self.normalized,
                        self.stride,
                        ptr::null(),
                    );
                }
            }
        }
    };
}

impl_attrib_enable!(Point3<f64>, 3, gl::DOUBLE);
impl_attrib_enable!(Point3<f32>, 3, gl::FLOAT);
impl_attrib_enable!(Point2<f64>, 2, gl::DOUBLE);
impl_attrib_enable!(Point2<f32>, 2, gl::FLOAT);
impl_attrib_enable!(f64, 1, gl::DOUBLE);
impl_attrib_enable!(f32, 1, gl::FLOAT);

impl<'a> UniformLocation<'a> {
    pub fn bind_buffer<'b, T>(self, buffer: &'b UniformBuffer<T>) -> Uniform<'a, 'b, T> {
        Uniform {
            id: self.id,
            // `clone` needs because of `impl Drop`
            name: self.name.clone(),
            shader: self.shader,
            buffer,
        }
    }
}

impl Drop for UniformLocation<'_> {
    fn drop(&mut self) {
        self.shader.drop_location(&self.name);
    }
}

impl<T: ?Sized> Drop for Uniform<'_, '_, T> {
    fn drop(&mut self) {
        self.shader.drop_location(&self.name);
    }
}

macro_rules! impl_uniform_enable {
    ($rs_type:ty, $gl_func:path, $($args:expr,)*) => {
        impl Uniform<'_, '_, $rs_type> {
            pub fn enable(&self) {
                unsafe {
                    $gl_func(self.id, $($args,)*);
                }
            }
        }
    };
}

impl_uniform_enable!(
    Matrix4<f64>,
    gl::UniformMatrix4dv,
    1,
    gl::FALSE,
    ptr::null(),
);
impl_uniform_enable!(
    Matrix4<f32>,
    gl::UniformMatrix4fv,
    1,
    gl::FALSE,
    ptr::null(),
);
impl_uniform_enable!(
    Matrix3<f64>,
    gl::UniformMatrix3dv,
    1,
    gl::FALSE,
    ptr::null(),
);
impl_uniform_enable!(
    Matrix3<f32>,
    gl::UniformMatrix3fv,
    1,
    gl::FALSE,
    ptr::null(),
);
