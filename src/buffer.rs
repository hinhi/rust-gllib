use std::marker::PhantomData;
use std::mem::size_of;
use std::os::raw::c_void;

use cgmath::{BaseFloat, Matrix, Matrix4};
use gl;
use gl::types::*;

pub trait Buffer<T: AsBuffer + ?Sized> {
    fn buffer_id(&self) -> GLuint;

    fn buffer_type(&self) -> GLenum;

    fn buffer_usage(&self) -> GLenum;

    fn bind(&self) {
        unsafe {
            gl::BindBuffer(self.buffer_type(), self.buffer_id());
        }
    }

    fn load(&self, data: &T) {
        let (size, ptr) = data.as_buffer();
        unsafe {
            self.bind();
            gl::BufferData(self.buffer_type(), size, ptr, self.buffer_usage());
        }
    }
}

pub trait AsBuffer {
    fn as_buffer(&self) -> (GLsizeiptr, *const c_void);
}

#[derive(Debug)]
pub struct ArrayBuffer<T: ?Sized> {
    id: GLuint,
    usage: GLenum,
    _type: PhantomData<T>,
}

#[derive(Debug)]
pub struct ElementBuffer<T: ?Sized> {
    id: GLuint,
    usage: GLenum,
    _type: PhantomData<T>,
}

#[derive(Debug)]
pub struct UniformBuffer<T: ?Sized> {
    id: GLuint,
    _type: PhantomData<T>,
}

#[derive(Debug, Copy, Clone)]
pub enum BufferUsage {
    Dynamic,
    Static,
}

impl From<BufferUsage> for GLenum {
    fn from(usage: BufferUsage) -> GLenum {
        match usage {
            BufferUsage::Dynamic => gl::DYNAMIC_DRAW,
            BufferUsage::Static => gl::STATIC_DRAW,
        }
    }
}

fn gen_buffer() -> GLuint {
    let mut id = 0;
    unsafe {
        gl::GenBuffers(1, &mut id);
    }
    id
}

impl<T: ?Sized> ArrayBuffer<T> {
    pub fn new(usage: BufferUsage) -> ArrayBuffer<T> {
        ArrayBuffer {
            id: gen_buffer(),
            usage: usage.into(),
            _type: PhantomData,
        }
    }
}

impl<T: ?Sized> ElementBuffer<T> {
    pub fn new(usage: BufferUsage) -> ElementBuffer<T> {
        ElementBuffer {
            id: gen_buffer(),
            usage: usage.into(),
            _type: PhantomData,
        }
    }
}

impl<T: ?Sized> UniformBuffer<T> {
    pub fn new() -> UniformBuffer<T> {
        UniformBuffer {
            id: gen_buffer(),
            _type: PhantomData,
        }
    }
}

impl<T: AsBuffer + ?Sized> Buffer<T> for ArrayBuffer<T> {
    fn buffer_id(&self) -> GLuint {
        self.id
    }

    fn buffer_type(&self) -> GLenum {
        gl::ARRAY_BUFFER
    }

    fn buffer_usage(&self) -> GLenum {
        self.usage
    }
}

impl<T: AsBuffer + ?Sized> Buffer<T> for ElementBuffer<T> {
    fn buffer_id(&self) -> GLuint {
        self.id
    }

    fn buffer_type(&self) -> GLenum {
        gl::ELEMENT_ARRAY_BUFFER
    }

    fn buffer_usage(&self) -> GLenum {
        self.usage
    }
}

impl<T: AsBuffer + ?Sized> Buffer<T> for UniformBuffer<T> {
    fn buffer_id(&self) -> GLuint {
        self.id
    }

    fn buffer_type(&self) -> GLenum {
        gl::UNIFORM_BUFFER
    }

    fn buffer_usage(&self) -> GLenum {
        BufferUsage::Dynamic.into()
    }
}

impl<T: ?Sized> Drop for ArrayBuffer<T> {
    fn drop(&mut self) {
        unsafe { gl::DeleteBuffers(1, &self.id as *const GLuint) }
    }
}

impl<T: ?Sized> Drop for ElementBuffer<T> {
    fn drop(&mut self) {
        unsafe { gl::DeleteBuffers(1, &self.id as *const GLuint) }
    }
}

impl<T: ?Sized> Drop for UniformBuffer<T> {
    fn drop(&mut self) {
        unsafe { gl::DeleteBuffers(1, &self.id as *const GLuint) }
    }
}

impl<T> AsBuffer for [T] {
    fn as_buffer(&self) -> (GLsizeiptr, *const c_void) {
        let size = (self.len() * size_of::<T>()) as GLsizeiptr;
        let data = &self[0] as *const T as *const c_void;
        (size, data)
    }
}

impl<S: BaseFloat> AsBuffer for Matrix4<S> {
    fn as_buffer(&self) -> (GLsizeiptr, *const c_void) {
        (
            size_of::<Self>() as GLsizeiptr,
            self.as_ptr() as *const c_void,
        )
    }
}
