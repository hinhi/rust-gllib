use cgmath::{BaseFloat, Point3, Vector3};

pub fn cube<S: BaseFloat>(center: Point3<S>, size: S) -> (Vec<Point3<S>>, Vec<usize>) {
    let unit = S::from(0.5).unwrap();
    let vertices = vec![
        center + Vector3::new(-unit, -unit, -unit) * size,
        center + Vector3::new(unit, -unit, -unit) * size,
        center + Vector3::new(unit, unit, -unit) * size,
        center + Vector3::new(-unit, unit, -unit) * size,
        center + Vector3::new(-unit, -unit, unit) * size,
        center + Vector3::new(unit, -unit, unit) * size,
        center + Vector3::new(unit, unit, unit) * size,
        center + Vector3::new(-unit, unit, unit) * size,
    ];
    let indices = vec![
        0, 2, 1, 0, 3, 2, 0, 5, 4, 0, 1, 5, 0, 7, 3, 0, 4, 7, 6, 4, 5, 6, 7, 4, 6, 1, 2, 6, 5, 1,
        6, 3, 7, 6, 2, 3,
    ];
    (vertices, indices)
}
